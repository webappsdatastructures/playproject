package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import javax.inject._
import play.api._
import play.api.mvc._
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.data._
import play.api.data.Forms._
import play.api.i18n.I18nSupport
import play.api.i18n.MessagesApi

import play.api.db.slick.DatabaseConfigProvider
import slick.driver.JdbcProfile
import slick.dbio.DBIOAction
import models._
import models.Tables._
import slick.driver.MySQLDriver.api._
import scala.concurrent.duration._
import scala.concurrent.{ Await, Future }

@Singleton
class TestController @Inject() (dbConfigProvider: DatabaseConfigProvider) extends Controller {

  val dbConfig = dbConfigProvider.get[JdbcProfile]
  
  def index() = Action {
    Ok(views.html.developerTest())
  }
  
  def saveTest(desc:String,pf:String) = Action { request =>
    println(desc+pf)
    val query = Tables.Test += Tables.TestRow(0,request.session.data("UserID").toInt,Option(desc),pf(0).toChar)
    dbConfig.db.run(query)
    Ok("Test Saved")
  }
  
}