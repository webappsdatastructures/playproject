package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.data._
import play.api.data.Forms._
import play.api.i18n.I18nSupport
import play.api.i18n.MessagesApi

import play.api.db.slick.DatabaseConfigProvider
import slick.driver.JdbcProfile
import slick.dbio.DBIOAction
import models._
import models.Tables._
import slick.driver.MySQLDriver.api._
import scala.concurrent.duration._
import scala.concurrent.{ Await, Future }
////import javax.persistence.*
//import org.mindrot.jbcrypt.BCrypt
//import play.data.validation.Constraints.Required
////import play.db.ebean.Model

//Form classes
case class UserPass(username: String, password: String)

@Singleton
class LoginController @Inject() (dbConfigProvider: DatabaseConfigProvider) extends Controller {

  val dbConfig = dbConfigProvider.get[JdbcProfile]
  //Forms' Instances
  val loginForm = Form(
    mapping(
      "username" -> text,
      "password" -> text)(UserPass.apply)(UserPass.unapply))

  val newUserForm = Form(
    mapping(
      "username" -> text,
      "password" -> text)(UserPass.apply)(UserPass.unapply))

  def index() = Action {
    Ok(views.html.login())
  }

  def createAccount() = Action.async { implicit request =>

    newUserForm.bindFromRequest().fold(
      badForm => {
        null //BadRequest(views.html.login()) 
      },
      goodForm => {

        val exists = dbConfig.db.run(User.filter(i => (i.username === goodForm.username) && (i.password === goodForm.password)).result)
        exists.map { i =>
          var numRows = i.length
          println("numRows = " + numRows.toString())

          if (numRows > 0) {
            // println("verify user returns = " + i.toString())
            println("User  Found")
            var rec = i.head

            var userId = rec.id.toString()
            var userName = rec.username
            println("User Id = " + userId + " and User Name = " + userName)

            //Ok(views.html.userMenu())
            println("User Already exists")
            null //User Already exists
          } else {
            dbConfig.db.run(User += UserRow(0, goodForm.username, goodForm.password, 'F'))
            println("user created")
            Ok(views.html.login())
          }
        }
      })

  }
  def newAccount() = Action {
    Ok(views.html.newAccount())
  }
  def practicePage() = Action { implicit request =>
    Ok(views.html.practicePage(request.session.data("Name")))
  }

  def testPage() = Action.async { implicit request =>
    Future(Ok(views.html.testPage(request.session.data("Name"))))
  }
//  def userMenu() = Action.async { implicit request =>
//
//    loginForm.bindFromRequest().fold(
//      badForm => {
//      },
//        null //BadRequest(views.html.login()) 
//      goodForm => {
//        //          val exists = dbConfig.db.run(User.filter(i => i.username === goodForm.username).exists.result)
//        //                    println("there")
//        //          exists.map{ i =>
//        //             if(i){
//        //              println("verify user returns = " + i.toString())
//        //                println("User  Found")
//        //               Ok(views.html.userMenu())//.withSession("UserID" -> i.get.id.toString())
//        //            }
//        //            else{
//        //              println("User Not Found")
//        //              Ok(views.html.login())
//        //            }                  
//        //          }
//
//        val exists = dbConfig.db.run(User.filter(i => (i.username === goodForm.username) && (i.password === goodForm.password)).result)
//        exists.map { i =>
//          var numRows = i.length
//          println("numRows = " + numRows.toString())
//
//          if (numRows > 0) {
//            // println("verify user returns = " + i.toString())
//            println("User  Found")
//            var rec = i.head
//            var userId = rec.id.toString()
//            var userName = rec.username
//            println("User Id = " + userId + " and User Name = " + userName)
//            val query = Tables.Practicepage.filter(i => i.userid === userId.toInt).result
//            val practicePages = dbConfig.db.run(query)
//            practicePages.map(i => Ok(views.html.userMenu(i.seq)).withSession("UserId" -> userId, "Name" -> userName))
//            //                Ok(views.html.userMenu(Await.result(practicePages, Duration.Inf))).withSession("UserId" -> userId, "Name" -> userName)
//          } else {
//            println("User Not Found")
//            Ok(views.html.login())
//          }
//        }
//      })
//    //Ok(views.html.userMenu())
//  }

  def toMenuWeGo() = Action.async { implicit request =>
    loginForm.bindFromRequest().fold(
      badForm => {
        null //BadRequest(views.html.login()) 
      },
      goodForm => {
        val exists = dbConfig.db.run(User.filter(i => (i.username === goodForm.username) && (i.password === goodForm.password)).result)
        exists.map(i => {
          if(i.length>0) {
            Ok(views.html.userMenu(goodForm.username)).withSession("UserID" -> i.head.id.toString(), "Name" -> i.head.username)
          } else {
            Ok(views.html.login())
          }
        })
      })
  }
  
  def getPracticePages() = Action.async{ implicit request => 
    println("Calling")

       val query = Practicepage.filter(i => i.userid === request.session.data("UserID").toInt).result
       val ret = dbConfig.db.run(query)
       ret.map(i => Ok(views.html.practiceMenus(i)).withSession(request.session))
  }
  
  def getTestPages() = Action.async{ implicit request => 
       val query = Test.filter(i => i.userid === request.session.data("UserID").toInt).result
       val ret = dbConfig.db.run(query)
       ret.map(i => Ok(views.html.testMenus(i)).withSession(request.session))
  }
  //def userMenu(data:String) = Action.async { implicit request =>

  //}
  def cheat() = Action {
    Ok(views.html.userMenu("cheater"))
  }
  def savePracticePage(id:String, pagename:String, structureinfo:String) = Action.async { implicit request => 
       println(structureinfo)
       dbConfig.db.run(Practicepage += PracticepageRow(id.toInt, request.session.data("UserID").toInt, pagename, Some(structureinfo), 'F'))
       scala.concurrent.Future(Ok("Page added!"))
  }
  def logout() = Action {
    println("here2")
    Ok("Bye").withNewSession
    // Ok(views.html.login())
    Redirect("http://dias11.cs.trinity.edu:9150")
  }

}