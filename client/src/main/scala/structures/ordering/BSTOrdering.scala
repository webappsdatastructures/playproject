package structures.ordering

import structures.node.Node

class BSTOrdering extends Order {
    def insert[T <: Node](elem: Double, array: collection.mutable.Buffer[T], hardIndex: Int = -1):Int = ???
  def remove[T <: Node](array:collection.mutable.Buffer[T]):Int = ???
  def print:String  = ???
  def describe:String = ???
}