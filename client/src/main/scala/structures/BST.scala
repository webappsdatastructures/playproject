package structures

//import structures.ordering.Order

//import scala.scalajs.js.JSApp
import org.scalajs.dom
import org.scalajs.dom.html
import dom.document
//import scala.scalajs.js.annotation.JSExportTopLevel
//import org.scalajs.jquery.jQuery

import structures.node.BSTNode

class BST(override var x: Double, override var y: Double) extends DataStructure {
  
  type NodeType = BSTNode
  var root: BSTNode = null
  private var nodeDistX = 40
  private var nodeDistY = 50
  private var bstStr = ""


  def push(data: Double, index: Int = -1, x: Int = 0, y: Int = 0, diff: Int = 0): Unit = {
    if (index != -1) {
      var newNode = new BSTNode(x, y, data)
      var old: BSTNode = children(index)
      if (diff > 0) {
        old.right = newNode
        old.right.parent = old
      } else {
        old.left = newNode
        old.left.parent = old
      }
      children += newNode
      println(children.map(i => i.data.toString()).mkString(","))
      return
    }
    if (root == null) {
      var newNode = new BSTNode(this.x, this.y, data)
      root = newNode
      children += root
      moveNodes()
      return
    }

    var curr = root
    val yOffset = 15
    while (curr != null) {
      if (data == curr.data) {
        //Can't insert duplicates
        moveNodes()
        return
      } else if (data < curr.data) {
        if (curr.left == null) {
          curr.left = new BSTNode(curr.getX - nodeDistX, curr.getY + nodeDistY+yOffset, data)
          curr.left.parent = curr
          children += curr.left
          moveNodes()
          return
        } else {
          curr = curr.left
        }
      } else {
        //(data > curr.data)
        if (curr.right == null) {
          curr.right = new BSTNode(curr.getX + nodeDistX, curr.getY + nodeDistY+yOffset, data)
          curr.right.parent = curr
          children += curr.right

          moveNodes()
          return
        } else {
          curr = curr.right
        }
      }
    }
    moveNodes()
  }

  def moveNodes() = {
    var flag = true

    for (i <- children.reverse) {
      for (j <- children.reverse) {
        if (i != j && i.getX == j.getX && i.getY == j.getY && flag) {
          flag = false
          var mid = i.getX
          for (x <- children) {
            if (x.getX < mid) {
              x.updatePos(x.getX - 40, x.getY)
            }
          }
          for (y <- children) {
            if (y.getX > mid) {
              y.updatePos(y.getX + 40, y.getY)
            }
          }
          if (i.data < j.data) {
            i.updatePos(i.getX - 40, i.getY)
            j.updatePos(j.getX + 40, j.getY)
          } else {
            i.updatePos(i.getX + 40, i.getY)
            j.updatePos(j.getX - 40, j.getY);
          }
        }
      }
    }
  }

  def delete(self: BSTNode, data: Double): Boolean = { //self is root initially
    if (self == null) false
    if (data < self.data) {
      //traverse left
      delete(self.left, data)
    } else if (data > self.data) {
      //traverse right
      delete(self.right, data)
    } else {
      //delete value
      if ((self.left != null) && (self.right != null)) {
        var successor = findMinNode(self.right)
        self.data = successor.data
        delete(successor, successor.data)
      } else if ((self.left != null) && (self.right == null)) {
        replaceNodeInParent(self, self.left)
      } else if ((self.left == null) && (self.right != null)) {
        replaceNodeInParent(self, self.right)

      } else {
        replaceNodeInParent(self, null)
      }
      true// deleted?
    }
    false// not found?		
  }

  def findMinNode(self: BSTNode): BSTNode = {
    //  		if(self == null){ can't equal null
    //  			return null;
    //  		} 		
    var currNode = self
    while (currNode.left != null) {
      currNode = currNode.left
    }
    currNode

    //  		return null;
  }

  def replaceNodeInParent(self: BSTNode, newVal: BSTNode) {
    if (self.parent != null) {
      if (self.data == self.parent.left.data) {
        self.parent.left = newVal;
        children.remove(children.indexOf(self));
      } else {
        self.parent.right = newVal;
        children.remove(children.indexOf(self));
      }
    }
    if (newVal != null) {
      if (self.parent == null) root = newVal; //replace root?
      children.remove(children.indexOf(self));
      newVal.parent = self.parent;
    }
  }

  def printInOrder() = {
    if (root != null) {
      printInOrderHelper(root);
    }
  }
  def printInOrderHelper(currNode: BSTNode): Unit = {
    if (currNode.left != null) printInOrderHelper(currNode.left);
    println(currNode.data);
    if (currNode.right != null) printInOrderHelper(currNode.right);
  }

  def getInOrderStr() = {
    bstStr = "";
    if (root != null) {
      setBSTStr(root);
    }
    bstStr;
  }
  

  def setBSTStr(currNode: BSTNode): Unit = {
    if (currNode.left != null) setBSTStr(currNode.left);
    bstStr += currNode.data + ",";
    if (currNode.right != null) setBSTStr(currNode.right);
  }

  override def refresh(ctx: dom.CanvasRenderingContext2D, step: Int) = {
    if (root != null) {
      refreshInOrderHelper(root, ctx, step)
    }
  }

  def refreshInOrderHelper(currNode: BSTNode, ctx: dom.CanvasRenderingContext2D, step: Int): Unit = {
    if (currNode.left != null) refreshInOrderHelper(currNode.left, ctx, step)
    currNode.refresh(ctx, step)
    if (currNode.right != null) refreshInOrderHelper(currNode.right, ctx, step)
  }

  override def clear() = {
    root = null;
    children.clear()
  }

  def getNodes() = {
    children
  }

  override def equals(other: DataStructure):Boolean = {
    other match {
      case i:BST => getInOrderStr() == i.getInOrderStr()
      case _ => false
    }
  }

//  override def printChildren: String = {
//    var str = ""
//    var i = 0
//    for (i <- children) {
//      str = str + i.data.toString() + ","
//    }
//    str.slice(0,str.length-1)
//    str
//  }
  
  

  def pop():Unit = ???
  
  def insertArray(elems:Seq[Int]):Unit = {
    for(e <- elems.reverse) {
      push(e)
    }
  }

  def insertStatic(index:Int, elem:Int):Unit = ???
  
  
  override def printInfo: String = {
    "00"
  }

  def remove(elem:Int):Unit = {
    delete(root, elem)
  }
  override def describe:String = "Binary Search Tree"

}