package structures


import org.scalajs.dom


trait Drawable {
  def refresh(gc:dom.CanvasRenderingContext2D,step:Int):Unit
}