package structures.node

import org.scalajs.dom
import structures.Drawable

class Pointer(private var ox:Double, private var oy:Double, private var dx:Double, private var dy:Double) extends Drawable {
  private var fox = ox
  private var foy = oy
  private var fdx = dx
  private var fdy = dy
  
  def updateDest(x:Double,y:Double) = {
    fdx = x
    fdy = y
  }
  
  def updateOrigin(x:Double,y:Double) = {
    fox = x
    foy = y
  }
  
  override def refresh(gc: dom.CanvasRenderingContext2D, step: Int): Unit = {
    if (this.ox != this.fox) {
      if (this.ox > this.fox) {
        this.ox -= Math.min(step,this.ox - this.fox)
      }
      else {
        this.ox += Math.min(step,this.fox-this.ox)
      }
    }
    if (this.oy != this.foy) {
      if (this.oy > this.foy) {
        this.oy -= Math.min(step,this.oy - this.foy)
      }
      else {
        this.oy += Math.min(step,this.foy-this.oy)
      }
    }
    if (this.dx != this.fdx) {
      if (this.dx > this.fdx) {
        this.dx -= Math.min(step,this.dx - this.fdx)
      }
      else {
        this.dx += Math.min(step,this.fdx-this.dx)
      }
    }
    if (this.dy != this.fdy) {
      if (this.dy > this.fdy) {
        this.dy -= Math.min(step,this.dy - this.fdy)
      }
      else {
        this.dy += Math.min(step,this.fdy-this.dy)
      }
    }
    this.draw(gc)
  }
  
  def draw(gc: dom.CanvasRenderingContext2D):Unit = {
    gc.fillStyle = "black"
    gc.beginPath()
    gc.moveTo(this.ox,this.oy)
    gc.lineTo(this.dx,this.dy)
    gc.stroke()
    val dir = Math.atan2(this.ox-this.dx,-this.oy+this.dy)
    val topDir = dir - 3*3.14159265/4
    val lowDir = dir - 3.14159265/4
    val pointLength = 7
    gc.beginPath()
    gc.moveTo(this.dx,this.dy)
    gc.lineTo(pointLength*Math.cos(topDir)+this.dx,pointLength*Math.sin(topDir)+this.dy)
    gc.stroke()
    gc.beginPath()
    gc.moveTo(this.dx,this.dy)
    gc.lineTo(pointLength*Math.cos(lowDir)+this.dx,pointLength*Math.sin(lowDir)+this.dy)
    gc.stroke()
  }
}