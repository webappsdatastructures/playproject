package structures.node



import org.scalajs.dom
import org.scalajs.dom.html
import dom.document

class BSTNode (override protected var x:Double, override protected var y:Double, override var data:Double) extends Node {
  override protected var fx = x
  override protected var fy = y
  var left:BSTNode = null
  var right:BSTNode = null
  var parent:BSTNode = null
  pointers += new Pointer(getLeftAnchor()(0),getLeftAnchor()(1),getLeftAnchor()(0),getLeftAnchor()(1))
  pointers += new Pointer(getRightAnchor()(0),getRightAnchor()(1),getRightAnchor()(0),getRightAnchor()(1))

  def getRightAnchor() = {
    val anchor = collection.mutable.Buffer[Double](x+Node.size/2,y+Node.size)
    anchor
  }

  def getLeftAnchor()= {
    val anchor = collection.mutable.Buffer[Double](x-Node.size/2,y+Node.size)
    anchor
  }
  


  def getTopAnchor() = {
    val anchor = collection.mutable.Buffer[Double](x,y+Node.size/2)
    anchor
  }
  
  override  def refresh(context:dom.CanvasRenderingContext2D,step:Int) = {
    if(left != null) {
      pointers(0).updateOrigin(getLeftAnchor()(0),getLeftAnchor()(1)+5)
      pointers(0).updateDest(left.getTopAnchor()(0),left.getTopAnchor()(1));
    }
    else {
      pointers(0).updateOrigin(getLeftAnchor()(0),getLeftAnchor()(1)+5);
      pointers(0).updateDest(getLeftAnchor()(0),getLeftAnchor()(1)+5);
    }
    if(right != null) {
      pointers(1).updateOrigin(getRightAnchor()(0),getRightAnchor()(1)-5);
      pointers(1).updateDest(right.getTopAnchor()(0),right.getTopAnchor()(1));
    }
    else {
      pointers(1).updateOrigin(getRightAnchor()(0),getRightAnchor()(1)-5);
      pointers(1).updateDest(getRightAnchor()(0),getRightAnchor()(1)-5);
    }
    super.refresh(context,step);
    draw(context);
  }

  override def draw(gc: dom.CanvasRenderingContext2D): Unit = {
    gc.fillStyle = "black";
    val sz = Node.size / 2
    gc.beginPath()
    gc.moveTo(x - sz, y + sz)
    gc.lineTo(x - sz, y + sz + Node.size)
    gc.stroke()
    gc.beginPath()
    gc.moveTo(x - sz, y + sz)
    gc.lineTo(x - sz + Node.size, y + sz)
    gc.stroke()
    gc.beginPath()
    gc.moveTo(x - sz + Node.size, y + sz + Node.size)
    gc.lineTo(x - sz + Node.size, y + sz)
    gc.stroke()
    gc.beginPath()
    gc.moveTo(x - sz + Node.size, y + sz + Node.size)
    gc.lineTo(x - sz, y + sz + Node.size)
    gc.stroke()
    gc.fillText(data.toString(), x, y + sz + 30);
    for (i <- pointers) i.draw(gc)
  }
  
}