package structures

import org.scalajs.dom
import structures.ordering.Order
import structures.node.SLLNode
import structures.node.Node

class SLL(override var x: Double, override var y: Double, ordering: Order) extends DataStructure {

  type NodeType = SLLNode

  override def refresh(gc: dom.CanvasRenderingContext2D, step: Int): Unit = {
    for (child <- children) child.refresh(gc, step)
  }

  def push(data: Double, index: Int = -1, x: Int = 400, y: Int = 0, diff: Int = 0): Unit = {
    val node = new SLLNode(x, y, data)
    val ind = ordering.insert(data, children, index)
    if (ind == this.children.length) {
      node.deletePointer()
      if (this.children.length > 0) {
        this.children(this.children.length - 1).addPointer
      }
    }
    if (diff > 0) children.insert(ind+1,node) else children.insert(ind,node)
    movePointers
    updateChildrenPositions
  }

  def pop(): Unit = {
    val index = this.ordering.remove(children)
    if (this.children.length > 1) {
      if (index > 0) {
        this.children(index - 1).updateNext(this.children(index).getNext.getNext)
      }
      this.children.remove(index)
      if (this.children.length > 0) {
        this.children(this.children.length - 1).deletePointer
      }
      movePointers
    } else if (this.children.length == 1) {
      children.clear()
    }
    updateChildrenPositions
  }

  def insertArray(elems: Seq[Int]): Unit = {
    for (e <- elems) {
      this.push(e)
    }
  }

  def insertStatic(index: Int, elem: Int): Unit = {
    children.insert(index, new SLLNode(x, y, elem))
    movePointers
    updateChildrenPositions
  }

  override def size(): Int = children.length

  override def printInfo: String = {
    "1" + this.ordering.print;
  }
//  override def printChildren: String = {
//    var str = ""
//    for (i <- children) {
//      str = str + i.data + ","
//    }
//    return str.slice(0, str.length - 1);
//  }
  def remove(elem: Int): Unit = {

  }
  private def movePointers(): Unit = {
    for (c <- 0 until this.children.length - 1) {
      this.children(c).updateNext(this.children(c + 1))
    }
  }
  private def updateChildrenPositions(): Unit = {
    for (c <- 0 until this.children.length) {
      val midpt = this.children.length / 2
      val newX = (this.x + (c - midpt) * Node.size * 2) + (Node.size * 2 / (midpt % 2 + 1))
      this.children(c).updatePos(newX, this.y)
    }
  }

  override def describe: String = ordering.describe + "Singly Linked List"

}