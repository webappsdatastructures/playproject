package structures

import org.scalajs.dom

import structures.node.Node

trait DataStructure extends Drawable {

  type NodeType <: Node
  val children = collection.mutable.Buffer[NodeType]()
  var x: Double
  var y: Double

  override def refresh(gc: dom.CanvasRenderingContext2D, step: Int): Unit = {
    for (child <- children) child.refresh(gc, step)
  }

  def push(data: Double, index: Int = -1, x: Int = 0, y: Int = 0, diff: Int = 0): Unit

  def pop(): Unit

  def insertArray(elems: Seq[Int]): Unit

  def insertStatic(index: Int, elem: Int): Unit

  def size(): Int = children.length

  def babies: List[NodeType] = children.toList

  def clear(): Unit = children.clear()
  def describe: String

  def equals(other: DataStructure): Boolean = {
    children.map(i => i.data.toInt) == other.children.map(i => i.data.toInt)
  }

  def neq(other: DataStructure): Boolean = {
    children.map(i => i.data.toInt) != other.children.map(i => i.data.toInt)
  }

  def printInfo: String
  def printChildren: String = children.map(i => i.data.toString).mkString(",")
  def remove(elem: Int): Unit
}

