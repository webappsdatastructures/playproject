package example

import scala.scalajs.js
import scala.scalajs.js.JSApp
import scala.scalajs.js.annotation.JSExportTopLevel
import scala.scalajs.js.timers.setInterval
import org.scalajs.dom
import org.scalajs.dom.document
import org.scalajs.dom.html
import org.scalajs.dom.raw.Element
import org.scalajs.jquery.jQuery
import app.CanvasHandler
import app.PracticeHandler
import app.TestHandler
import org.scalajs.jquery.{ jQuery => JQ, JQueryAjaxSettings, JQueryXHR }

object ClientMain extends JSApp {
  private var practiceFile:String = null
  def main(): Unit = {
    //val canvas = document.getElementById("testCanvas").asInstanceOf[html.Canvas]
    //val canvHandler = new CanvasHandler(canvas)
  }
  @JSExportTopLevel("menu")
  def menu(): Unit = {
    dom.window.location.reload()
  }
  @JSExportTopLevel("logout")
  def logout(): Unit = {
    println("logging out")
    dom.window.location.assign("http://dias11.cs.trinity.edu:9150/logout")
    // jQuery.get("/logout")
  }
  @JSExportTopLevel("practice")
  def practice():Unit = {
    jQuery("#menuoptions").hide()
    println("calling here")
    jQuery("#practiceoptions").show()
    jQuery("#practicetable").load("/getPractices")
  }
  @JSExportTopLevel("test")
  def test():Unit = {
    jQuery("#menuoptions").hide()
    jQuery("#testoptions").show()
    jQuery("#testtable").load("/getTests")
  }
  def practiceHelper(str: String, str2: String, jqXHR: JQueryXHR): Unit = {
    val practiceHandler = new PracticeHandler
    practiceHandler.start()
  }
  @JSExportTopLevel("newPractice")
  def newPractice(): Unit = {
    jQuery("#body").load("/practicePage", complete = practiceHelper _)
  }
  def loadPracticeHelper(str: String, str2: String, jqXHR: JQueryXHR): Unit = {
    val practiceHandler = new PracticeHandler
    practiceHandler.start()
    if(practiceFile!=null) {
      practiceHandler.loadPracticePage(practiceFile)
    }
  }
  @JSExportTopLevel("loadPractice")
  def loadPractice(data:String):Unit = {
    practiceFile = data
    jQuery("#body").load("/practicePage", complete = loadPracticeHelper _)
  }  
  def testHelper(str: String, str2: String, jqXHR: JQueryXHR): Unit = {
    val testHandler = new TestHandler
    testHandler.start()
  }
  @JSExportTopLevel("newTest")
  def newTest(): Unit = {
    jQuery("#body").load("/testPage", complete = testHelper _)
  }
}
