package app

import scala.scalajs.js
import scala.scalajs.js.JSApp
import scala.scalajs.js.annotation.JSExportTopLevel
import scala.scalajs.js.timers.setInterval

import org.scalajs.dom
import org.scalajs.dom.document
import org.scalajs.dom.html
import org.scalajs.dom.raw.Element
import org.scalajs.jquery.jQuery
import structures.node.Node
import structures.node.SLLNode
import structures.DataStructure
import structures.Drawable
import structures.DLL
import structures.SLL
import structures.BST
import structures.ordering.Order
import structures.ordering.StackOrdering
import structures.ordering.QueueOrdering
import structures.ordering.PriorityQueueOrdering
import org.scalajs.dom.html.Canvas
import org.scalajs.jquery.jQuery
import org.scalajs.dom.raw.HTMLElement
import scala.scalajs.js.annotation.JSExportTopLevel
import scala.util.Try

class PracticeHandler {
  var ordering:Order = null
  var DS:DataStructure = null
  var listType:Int = 0
  var bst = false
  var pageName:String = null
  var structId:Int = -1
  var viewMode = false
  def start():Unit = {
    val canvas = document.getElementById("canv").asInstanceOf[html.Canvas]  
    val gc: dom.CanvasRenderingContext2D = canvas.getContext("2d").asInstanceOf[dom.CanvasRenderingContext2D]
    val step:Int = 5
    var fileName:String = ""
    hidePop()
    setInterval(50) {drawStructure(DS)}  
    def drawStructure(structure:Drawable):Unit = {
      if(DS!=null) {
        gc.clearRect(0, 0, canvas.width, canvas.height)
        structure.refresh(gc,step)
      }
    }
    jQuery("#push").click(() => {
      val node = Try(jQuery("#newnode").value.asInstanceOf[String].toInt).toOption
      if(node!=None) {
        push(node.get)
      }
      jQuery("#newnode").value("")
    })
    jQuery("#pop").click(() => {
      if(bst) {
        val node = Try(jQuery("#oldnode").value.asInstanceOf[String].toInt).toOption
        if(node!=None) {
          DS.remove(node.get)
        }
      } else {
        pop()
      }
      jQuery("#oldnode").value("")
    })
    jQuery("#clear").click(() => {
      clear()
    })
    jQuery("#stack").click(() => {
      jQuery("#structurebutton").text(jQuery("#stack").text)
      ordering = new StackOrdering()
    })
    jQuery("#queue").click(() => {
      jQuery("#structurebutton").text(jQuery("#queue").text)
      ordering = new QueueOrdering()
    })
    jQuery("#pq").click(() => {
      jQuery("#structurebutton").text(jQuery("#pq").text)
      ordering = new PriorityQueueOrdering()
    })
    jQuery("#bst").click(() => {
      jQuery("#structurebutton").text(jQuery("#bst").text)
      ordering = new StackOrdering()
      listType = 3
    })
    jQuery("#slinked").click(() => {
      jQuery("#listbutton").text(jQuery("#slinked").text)
      if(listType!=3) listType = 1
    })
    jQuery("#dlinked").click(() => {
      jQuery("#listbutton").text(jQuery("#dlinked").text)
      if(listType!=3) listType = 2
    })
    jQuery("#createbutton").click(() => {
      create()
    })
    jQuery("#savebutton").click(() => {
      val newFileName = jQuery("#filename").value.asInstanceOf[String]
      if(newFileName!="") {
        println(newFileName)
        if(DS!=null) {
          val si = Parsing.structureToString(DS, 0, newFileName)
          println(si)
          jQuery.get("/savePracticePage"+"/1/"+newFileName+"/"+si)
        }
        jQuery("#filename").value("")
      }
    })
  }
  def clear():Unit = {
    DS.clear()
  }
  def push(elem:Int):Unit = {
    if(DS!=null) {
      DS.push(elem.toInt)
    }
  }
  def pop():Unit = {
    if(DS!=null) {
        DS.pop()
    }
  }
  def create():Unit = {
    if(ordering!=null && listType!=0) {
      listType match {
        case 1 => {
          DS = new SLL(400, 200, ordering)
          bst = false
          hidePop()
        }
        case 2 => {
          DS = new DLL(400, 200, ordering)
          bst = false
          hidePop()
        }
        case 3 => {
          DS = new BST(400, 200)
          bst = true
          showPop()
        }
      }
      listType = 0
    }
    restoreDefaults()
  }
  def restoreDefaults():Unit = {
    jQuery("#structurebutton").text("Structure")
    jQuery("#listbutton").text("List")
  }
  def hidePop():Unit = {
    jQuery("#oldnode").hide()
    jQuery("#pop").text("Pop")
  }
  def showPop():Unit = {
    jQuery("#oldnode").show()
    jQuery("#pop").text("Remove")
  }
  def loadPracticePage(data:String):Unit = {
    viewMode = true
    println(data)
    val tmp = Parsing.stringToStructure(data)
    DS = tmp._1
    structId = tmp._2
    pageName = tmp._3
    jQuery("#filename").value(pageName)
    jQuery("#filename").prop("disabled", true)
    jQuery("#savebutton").prop("disabled", true)
  }
}