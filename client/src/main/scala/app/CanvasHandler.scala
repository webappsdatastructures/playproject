package app

import scala.scalajs.js
import scala.scalajs.js.JSApp
import scala.scalajs.js.annotation.JSExportTopLevel
import scala.scalajs.js.timers.setInterval

import org.scalajs.dom
import org.scalajs.dom.document
import org.scalajs.dom.html
import org.scalajs.dom.raw.Element
import org.scalajs.jquery.jQuery
import structures.DataStructure
import structures.Drawable
import structures.DLL
import structures.SLL
import structures.ordering.StackOrdering
import structures.DLL
import structures.BST

class CanvasHandler(canvas:html.Canvas) {
  private val gc: dom.CanvasRenderingContext2D = canvas.getContext("2d").asInstanceOf[dom.CanvasRenderingContext2D]
  private val step:Int = 5

  /******** FOR TESTING PURPOSES CHANGE THESE VARIABLES. WON'T EXIST IN THE FINAL VERSION ********/

  val ordering = new StackOrdering()
  val DS = new DLL(400,400,ordering)
  DS.push(4)
  DS.push(3)
  DS.push(6)
  DS.push(7)
  DS.push(2)

  setInterval(50) {drawStructure(DS)}
    
  /**********************************************************************************************/
  
  def drawStructure(structure:Drawable):Unit = {
    gc.clearRect(0, 0, canvas.width, canvas.height)
    structure.refresh(gc,step)
  }
  
  private def shiftx: Double = 0
  private def shifty: Double = 0
}