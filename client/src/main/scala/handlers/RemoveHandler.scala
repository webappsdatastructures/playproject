package handlers

trait RemoveHandler {
  def execute():Unit
  def undo:Unit
}